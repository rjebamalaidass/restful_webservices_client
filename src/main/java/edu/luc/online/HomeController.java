package edu.luc.online;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import edu.luc.webservices.bean.ProductOrder;
import edu.luc.webservices.bean.WebAddress;
import edu.luc.webservices.bean.WebOrder;

@Controller

public class HomeController {
	
	@Autowired
	ProductService productService;
	
	@GetMapping(value="home")
	public String getHome(Model model, HttpServletRequest request) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		model.addAttribute("productlist", productService.getProductList("http://localhost:8081/product/productlist"));
	
		return "home";
	}
	
	@GetMapping(value="getproductdetail")
	public String getproductdetail(Model model, HttpServletRequest request) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		String url = (String)request.getParameter("url");
		
		model.addAttribute("detail", productService.getProductDetail(url));
		model.addAttribute("productlist", productService.getProductList("http://localhost:8081/product/productlist"));
		
		return "home";
	}
	
	@GetMapping(value="getorderlist")
	public String getorderlist(Model model, HttpServletRequest request) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		
		String userid = (String)request.getSession().getAttribute("userid");
		System.out.println("in get order list with user id "+userid);
		model.addAttribute("orderlist", productService.getOrderList("http://localhost:8084/report/orderdetails/"+userid+"/status"));
		return "orderlist";
	}
	
	@PostMapping(value="getorderCancel")
	public String orderCancel(@RequestParam("url") String url, Model model, HttpServletRequest request)  throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		String userid = (String)request.getSession().getAttribute("userid");
		System.out.println("in get order list with user id "+userid);
		productService.getCancel(url);
		model.addAttribute("orderlist", productService.getOrderList("http://localhost:8084/report/orderdetails/"+userid+"/status"));
		return "orderlist";
	}
	
	@PostMapping(value="update")
	public String update(@RequestParam("quantity") String quantity,
			@RequestParam("address1") String address1,
			@RequestParam("address2") String address2,
			@RequestParam("city") String city,
			@RequestParam("state") String state,
			@RequestParam("zip") String zip,
			@RequestParam("updateURL") String updateURL, @RequestParam("shippingURL") String shippingURL, Model model, HttpServletRequest request) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		try
		{
		
		
		System.out.println(quantity+" - "+address1+" - "+address2+" - "+city+" - "+state+" - "+zip+" - "+updateURL+" - "+shippingURL);
		System.out.println("the ordering URL is "+updateURL);
		
		
		productService.updateProduct(updateURL, shippingURL, quantity, address1, address2, city, state, zip);
		
	
		model.addAttribute("errorMessage", "Quantity and address was updated successfully");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			model.addAttribute("errorMessage", "order failed due to exception");
		}
		String userid = (String)request.getSession().getAttribute("userid");
		System.out.println("in get order list with user id "+userid);
		model.addAttribute("orderlist", productService.getOrderList("http://localhost:8084/report/orderdetails/"+userid+"/status"));
		return "orderlist";
	}
	
	@PostMapping(value="order")
	public String order(@RequestParam("productIdOrder") String id, @RequestParam("quantity") String quantity,
			@RequestParam("address1") String address1,
			@RequestParam("address2") String address2,
			@RequestParam("city") String city,
			@RequestParam("state") String state,
			@RequestParam("zip") String zip,
			@RequestParam("orderingURL") String orderingURL, Model model, HttpServletRequest request) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		try
		{
		
		
		System.out.println(id+" - "+quantity+" - "+address1+" - "+address2+" - "+city+" - "+state+" - "+zip+" - "+orderingURL);
		System.out.println("the ordering URL is "+orderingURL);
		
		String userid = (String)request.getSession().getAttribute("userid");
		System.out.println("the user id ordering is "+userid);
		orderingURL = orderingURL.replace("<%=USER_ID%>", userid);
		productService.orderProduct(orderingURL,  id, quantity, address1, address2, city, state, zip);
		
	
		model.addAttribute("errorMessage", "order for quantity "+quantity+" was placed successfully");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			model.addAttribute("errorMessage", "order failed due to exception");
		}
		model.addAttribute("productlist", productService.getProductList("http://localhost:8081/product/productlist"));
		
		return "home";
	}
	
	

}
