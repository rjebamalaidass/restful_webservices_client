package edu.luc.online;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.xml.ws.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.luc.webservices.bean.Order;
import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.bean.ProductOrder;
import edu.luc.webservices.bean.WebAddress;
import edu.luc.webservices.bean.WebOrder;

@Service
public class ProductService {
	
	private WebClient getProductClient(String uri)
	{
		 List<Object> providers = new ArrayList<Object>();
		 JacksonJsonProvider provider = new JacksonJsonProvider();
         provider.addUntouchable(Response.class);
         providers.add(provider);
         
        
		/*****************************************************************************************
         * POST METHOD invoke
        *****************************************************************************************/
        System.out.println("POST METHOD .........................................................");
       
        WebClient postClient = WebClient.create(uri, providers);
      
        postClient = postClient.accept("application/json").type("application/json");
     	
        String postRequestURI = postClient.getCurrentURI().toString();
        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
        String postRequestHeaders = postClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
        
        return postClient;
	}
	
	
	private WebClient getOrderClient(String uri)
	{
		 List<Object> providers = new ArrayList<Object>();
		 JacksonJsonProvider provider = new JacksonJsonProvider();
         provider.addUntouchable(Response.class);
         providers.add(provider);         
        
		/*****************************************************************************************
         * POST METHOD invoke
        *****************************************************************************************/
        System.out.println("POST METHOD .........................................................");
        //WebClient postClient = WebClient.create("http://localhost:8082/order/", providers);
        WebClient postClient = WebClient.create(uri, providers);
       // WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
       // WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        //postClient = postClient.accept("application/json").type("application/json").path(uri);
        postClient = postClient.accept("application/json").type("application/json");
     	
        String postRequestURI = postClient.getCurrentURI().toString();
        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
        String postRequestHeaders = postClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
        
        return postClient;
	}
	
	public List<ProductBean> getProductList(String uri) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		WebClient client = getProductClient(uri);
		String response =  client.get(String.class);
		
		System.out.println(response);
		Class<?> clazz = Class.forName("edu.luc.webservices.bean.ProductBean");

		System.out.println(String.format("Mapping class %s to JSON %s", clazz, response));

		//Object data = new ObjectMapper().readValue(response, clazz);
		List<LinkedHashMap> data = new ObjectMapper().readValue(response, List.class);
		System.out.println("the product list output is "+data.toString());
		List<ProductBean> beanList = new ArrayList<ProductBean>();
		for(LinkedHashMap map : data)
		{
			ProductBean productBean = new ProductBean();
			Integer productId = (Integer)map.get("productid");
			String productName  = (String)map.get("productname");
			String description = (String)map.get("description");
			String category = (String)map.get("category");
			Double price = (Double)map.get("price");
			Integer quantity = (Integer)map.get("availablequantity");
			productBean.setProductid(productId);
			productBean.setProductname(productName);
			productBean.setDescription(description);
			productBean.setCategory(category);
			productBean.setPrice(price);
			productBean.setAvailablequantity(quantity);
			
			List li = (List)map.get("links");
			if(li!=null)
			{
				HashMap<String, String> actionmap = new HashMap<String, String>();
				for(int i=0;i<li.size();i++)
				{
					System.out.println(li.get(i));
					LinkedHashMap listStr = (LinkedHashMap)li.get(i);
					System.out.println("the action is "+listStr.get("action"));
					System.out.println("the URL is "+listStr.get("url"));
					String action = (String)listStr.get("action");
					String url = (String)listStr.get("url");
					if(StringUtils.equalsAnyIgnoreCase(action,"detail"))
					{
						productBean.setDetailurl(url);
					}
					if(StringUtils.equalsAnyIgnoreCase(action,"order"))
					{
						productBean.setOrderurl(url);
					}
				}
				
			}
			beanList.add(productBean);
		}
		beanList.stream().forEach(b-> System.out.println(b.toString()));
		return beanList;
	
	}


	public ProductBean getProductDetail(String uri) throws JsonMappingException, JsonProcessingException, ClassNotFoundException{

		WebClient client = getProductClient(uri);
		String response =  client.get(String.class);
		
		System.out.println(response);
		Class<?> clazz = Class.forName("edu.luc.webservices.bean.ProductBean");

		System.out.println(String.format("Mapping class %s to JSON %s", clazz, response));

		//Object data = new ObjectMapper().readValue(response, clazz);
		ProductBean data = new ObjectMapper().readValue(response, ProductBean.class);
		System.out.println(data.toString());
		return data;
		
	}
	
	public void getCancel(String uri) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		WebClient client = getProductClient(uri);
		client.delete();
		
	}
	
	public List<Order> getOrderList(String uri)throws JsonMappingException, JsonProcessingException, ClassNotFoundException{

		WebClient client = getProductClient(uri);
		String response =  client.get(String.class);
		
		System.out.println(response);
		Class<?> clazz = Class.forName("edu.luc.webservices.bean.WebOrder");

		System.out.println(String.format("Mapping class %s to JSON %s", clazz, response));

		//Object data = new ObjectMapper().readValue(response, clazz);
		List<LinkedHashMap> data = new ObjectMapper().readValue(response, List.class);
		List<Order> orderList = new ArrayList<Order>();
		for(LinkedHashMap map : data)
		{
			Order order = new Order();
			String orderid = ((Integer)map.get("orderId")).toString();
			order.setOrderid(orderid);
			
			List pil = (List)map.get("productList");
			LinkedHashMap pi = (LinkedHashMap)pil.get(0);
			String productid = ((Integer)pi.get("productId")).toString();
			String quantity = ((Integer)pi.get("quantity")).toString();
			String price = ((Double)pi.get("price")).toString();
			String name = (String)pi.get("name");
			order.setProductid(productid);
			order.setQuantity(quantity);
			order.setPrice(price);
			order.setName(name);
			
			List pli = (List)pi.get("links");
			if(pli!=null)
			{
				
				for(int i=0;i<pli.size();i++)
				{
					System.out.println(pli.get(i));
					LinkedHashMap listStr = (LinkedHashMap)pli.get(i);
					System.out.println("the action is "+listStr.get("action"));
					System.out.println("the URL is "+listStr.get("url"));
					String action = (String)listStr.get("action");
					String url = (String)listStr.get("url");
					if(StringUtils.equalsAnyIgnoreCase(action,"detail"))
					{
						order.setProducturl(url);
					}
					if(StringUtils.equalsAnyIgnoreCase(action,"update"))
					{
						order.setUpdateurl(url);
					}
					
					
				}
				
			}
			
			
			LinkedHashMap ai = (LinkedHashMap)map.get("address");
			String addressid = ((Integer)ai.get("addressid")).toString();
			String addresstype = (String)ai.get("addresstype");
			//String userid = (String)ai.get("userid");
			String address1 = (String)ai.get("address1");
			String address2 = (String)ai.get("address2");
			String city = (String)ai.get("city");
			String state = (String)ai.get("state");
			String zip = (String)ai.get("zip");
			order.setAddressid(addressid);
			order.setAddresstype(addresstype);
			order.setAddress1(address1);
			order.setAddress2(address2);
			order.setCity(city);
			order.setState(state);
			order.setZip(zip);
			String orderstatus = (String)map.get("orderstatus");
			String orderprice = ((Double)map.get("orderprice")).toString();
			String shippingcost = ((Double)map.get("shippingcost")).toString();
			String totalprice = ((Double)map.get("totalprice")).toString();
			//String orderdate = (String)map.get("orderdate");
			order.setOrderstatus(orderstatus);
			order.setOrderprice(orderprice);
			order.setShippingcost(shippingcost);
			order.setTotalprice(totalprice);
			//order.setOrderdate(orderdate);
			
			
			List li = (List)map.get("links");
			if(li!=null)
			{
				
				for(int i=0;i<li.size();i++)
				{
					System.out.println(li.get(i));
					LinkedHashMap listStr = (LinkedHashMap)li.get(i);
					System.out.println("the action is "+listStr.get("action"));
					System.out.println("the URL is "+listStr.get("url"));
					String action = (String)listStr.get("action");
					String url = (String)listStr.get("url");
					if(StringUtils.equalsAnyIgnoreCase(action,"detail"))
					{
						order.setOrderdetailurl(url);
					}
					if(StringUtils.equalsAnyIgnoreCase(action,"payment"))
					{
						order.setPaymenturl(url);
					}
					if(StringUtils.equalsAnyIgnoreCase(action,"shipping"))
					{
						order.setShippingurl(url);
					}
					if(StringUtils.equalsAnyIgnoreCase(action,"cancel"))
					{
						order.setCancelurl(url);
					}
				}
				
			}
			orderList.add(order);
			
			
			
			
			
			
			
		}
		orderList.stream().forEach(s->System.out.println(s.toString()));
		return orderList;
		
	}


	public void orderProduct(String orderingURL, String id, String quantity, String address1, String address2,
			String city, String state, String zip) {	
		
		WebClient client = getOrderClient(orderingURL);
		WebAddress address = new WebAddress();
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setAddresstype("shipping");
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		WebOrder order = new WebOrder();
		order.setAddress(address);
		
		List<ProductOrder> productList = new ArrayList<ProductOrder>();
		ProductOrder po = new ProductOrder();
		po.setQuantity(Integer.parseInt(quantity));
		productList.add(po);
		order.setProductList(productList);
		
		
		
		String responsePost =  client.post(order, String.class);
		
	}
	
	

	public void updateProduct(String updateurl, String shippingurl , String quantity, String address1, String address2,
			String city, String state, String zip) {	
		
		WebClient client = getOrderClient(shippingurl);
		WebAddress address = new WebAddress();
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setAddresstype("shipping");
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		String responsePost =  client.post(address, String.class);
		
		WebClient updateClient = getOrderClient(updateurl);
		String responseupdate =  updateClient.post(Integer.parseInt(quantity), String.class);
		
	}
	
	public void updateQuantity(String url, String quantity)
	{
		WebClient updateClient = getOrderClient(url);
		String responseupdate =  updateClient.post(Integer.parseInt(quantity), String.class);
		
	}
	

	
	



}
