package edu.luc.online;

import javax.naming.directory.Attributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.configurers.ldap.LdapAuthenticationProviderConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;


/**

 * @author rjebamalaidass
 */
@Configuration

public class SecurityConfig extends WebSecurityConfigurerAdapter {

	
	Logger logger = LoggerFactory.getLogger(SecurityConfig.class);
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
			.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
		
			.antMatchers("/css/**", "/resources/**", "/images/**", "/js/**","/home","/productdetail").permitAll()
			
			.anyRequest().fullyAuthenticated().and().formLogin()
			.successHandler(authenticationSuccessHandler())
			.and().logout().permitAll();
			
	}
	
    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler(){
        return new SuccessAuthenticationHandler();
    }
	
   

		

		
		
		



	
	
	
}
