package edu.luc.online;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.Response;


import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.luc.webservices.bean.Link;
import edu.luc.webservices.bean.Login;
import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.bean.ProductOrder;
import edu.luc.webservices.bean.RegisterDetail;
import edu.luc.webservices.bean.WebAddress;
import edu.luc.webservices.bean.WebOrder;
/*import edu.luc.webservices.domain.Address;
import edu.luc.webservices.domain.Order;
import edu.luc.webservices.domain.Product;
import edu.luc.webservices.domain.User;
*/

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

@Service
public class WebServicesTestClient {
	
	public WebClient getProductListClient(String uri)
	{
		 List<Object> providers = new ArrayList<Object>();
		 JacksonJsonProvider provider = new JacksonJsonProvider();
         provider.addUntouchable(Response.class);
         providers.add(provider);
         
        
		/*****************************************************************************************
         * POST METHOD invoke
        *****************************************************************************************/
        System.out.println("POST METHOD .........................................................");
        WebClient postClient = WebClient.create("http://localhost:8081/product/", providers);
       // WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
       // WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        postClient = postClient.accept("application/json").type("application/json").path(uri);
     	
        String postRequestURI = postClient.getCurrentURI().toString();
        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
        String postRequestHeaders = postClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
        
        return postClient;
	}
	
	public WebClient getProductClient(String uri)
	{
		 List<Object> providers = new ArrayList<Object>();
		 JacksonJsonProvider provider = new JacksonJsonProvider();
         provider.addUntouchable(Response.class);
         providers.add(provider);
         
        
		/*****************************************************************************************
         * POST METHOD invoke
        *****************************************************************************************/
        System.out.println("POST METHOD .........................................................");
        WebClient postClient = WebClient.create(uri, providers);
       // WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
       // WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        //postClient = postClient.accept("application/json").type("application/json").path(uri);
        postClient = postClient.accept("application/json").type("application/json");
     	
        String postRequestURI = postClient.getCurrentURI().toString();
        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
        String postRequestHeaders = postClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
        
        return postClient;
	}
	
	
	public WebClient getOrderClient(String uri)
	{
		 List<Object> providers = new ArrayList<Object>();
		 JacksonJsonProvider provider = new JacksonJsonProvider();
         provider.addUntouchable(Response.class);
         providers.add(provider);         
        
		/*****************************************************************************************
         * POST METHOD invoke
        *****************************************************************************************/
        System.out.println("POST METHOD .........................................................");
        WebClient postClient = WebClient.create("http://localhost:8082/order/", providers);
       // WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
       // WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        postClient = postClient.accept("application/json").type("application/json").path(uri);
     	
        String postRequestURI = postClient.getCurrentURI().toString();
        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
        String postRequestHeaders = postClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
        
        return postClient;
	}
	
//	public User registerUser()
//	{
//		WebClient client = getClient("register");
//		 RegisterDetail register = new RegisterDetail();
//	        register.setEmail("rjebamalaidass@luc.edu");
//	        register.setFirstName("Rejoice");
//	        register.setFirstName("Jebamalaidass");
//	        register.setPhone("123-456-7894");
//	        register.setType("C");
//	        register.setAddressType("Home");
//	        register.setAddress1("1642 Loyola University");
//	        register.setAddress2("North Sheridan Road");
//	        register.setCity("Chicago");
//	        register.setState("IL");
//	        register.setZip("60089");
//	        
//	        
//	     	User responsePost =  client.post(register, User.class);
//	        System.out.println("POST MEDTHOD Response - Register ........." + responsePost.toString());
//	        return responsePost;
//	}
//	
//
//	private User setupLogin(int userid) {
//		WebClient client = getClient("setuplogin");
//		Login login = new Login();
//		login.setUserid(userid);
//		login.setUserName("abcusername");
//		login.setPassword("abcpassword");
//		User responsePost =  client.post(login, User.class);
//        System.out.println("POST MEDTHOD Response - Setup Login ........." + responsePost.toString());
//        return responsePost;
//		
//	}
//	
//	private String login()
//	{
//		WebClient client = getClient("login");
//		Login login = new Login();
//		login.setUserName("abcusername");
//		login.setPassword("abcpassword");
//		User responsePost =  client.post(login, User.class);
//        System.out.println("POST MEDTHOD Response - Authentication ........." + responsePost.toString());
//        return String.valueOf(responsePost.getUserid());
//	}
	
	private void getProductList() throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		WebClient client = getProductClient("http://localhost:8081/product");
		String response =  client.get(String.class);
		
		System.out.println(response);
		Class<?> clazz = Class.forName("edu.luc.webservices.bean.ProductBean");

		System.out.println(String.format("Mapping class %s to JSON %s", clazz, response));

		//Object data = new ObjectMapper().readValue(response, clazz);
		List<Object> data = new ObjectMapper().readValue(response, List.class);
		System.out.println("the product list output is "+data.toString());
		LinkedHashMap map = (LinkedHashMap)data.get(0);
		Set<String> sett = map.keySet();
		for(String set : sett)
		{
			//System.out.println(bean.getProductid()+" ---- "+bean.getProductname()+" ---- "+bean.getPrice());
			System.out.println(set);
			System.out.println(map.get(set));
			if(StringUtils.equalsIgnoreCase(set, "links"))
			{
				List li = (List)map.get(set);
				
				for(int i=0;i<li.size();i++)
				{
					System.out.println(li.get(i));
					LinkedHashMap listStr = (LinkedHashMap)li.get(i);
					System.out.println("the action is "+listStr.get("action"));
					System.out.println("the URL is "+listStr.get("url"));
				}
				
			}
		}
			}
	
	private void getCategoryList()
	{
		WebClient client = getProductClient("category");
		String response =  client.get(String.class);
		System.out.println(response);
	}
	
	private void getProductByCategory()
	{
		String category = "hardware";
		WebClient client = getProductClient("productbycategory/"+category);
		String response =  client.get(String.class);
		System.out.println(response);
	}
	
	private void getProdctById()
	{
		String productId = "53";
		WebClient client = getProductClient("productdetails/"+productId);
		String response =  client.get(String.class);
		System.out.println(response);
	}
	
//	private String addAddress(String userid)
//	{
//		WebClient client = getClient("addAddress");
//		Address address = new Address();
//		address.setAddresstype("PERMANENT");
//		address.setUserid(Integer.parseInt(userid));
//		address.setAddress1("First streeet Address");
//		address.setAddress2("Second Street Address");
//		address.setCity("Chicago");
//		address.setState("IL");
//		address.setZip("60660");
//		String responsePost =  client.post(address, String.class);
//        System.out.println("POST MEDTHOD Response - Add Address ........." + responsePost);
//        return responsePost;
//	}
//	
	private void addProductToCart()
	{
		String  userid = "69";
		WebClient client = getOrderClient("placeorder/"+userid);
		WebAddress address = new WebAddress();
		address.setAddress1("1234 testing the code");
		address.setAddress2("1234 second address");
		address.setAddresstype("shipping");
		address.setCity("Chicago");
		address.setState("IL");
		address.setZip("60089");
		WebOrder order = new WebOrder();
		order.setAddress(address);
		
		List<ProductOrder> productList = new ArrayList<ProductOrder>();
		ProductOrder po = new ProductOrder();
		po.setProductId(53);
		po.setQuantity(52);
		productList.add(po);
		po = new ProductOrder();
		po.setProductId(54);
		po.setQuantity(100);
		productList.add(po);
		order.setProductList(productList);
		
		
		
		String responsePost =  client.post(order, String.class);
        System.out.println("POST MEDTHOD Response - Add Cart ........." + responsePost);
		
	}
	
	private void getOrderDetail()
	{
		String orderId = "65";
		WebClient client = getOrderClient("orderDetail/"+orderId);
		String response =  client.get(String.class);
		System.out.println(response);
	}
	
	
	public static void main(String[] s) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		ProductService service = new ProductService();
		//service.getProductDetail("http://localhost:8081/product/productdetails/53");
		//service.getOrderList("http://localhost:8084/report/orderdetails/69/status");
		//service.updateQuantity("http://localhost:8082/order/updateOrder/87/53", "63");
		service.getProductDetail("http://localhost:8081/product/productdetails/53");
		
		
		
//		WebServicesTestClient test = new WebServicesTestClient();
//		
//		test.getProductList();
//		test.getCategoryList();
//		test.getProductByCategory();
//		test.getProdctById();
//		//test.addProductToCart();
//		test.getOrderDetail();
	  
	  
	}


}
