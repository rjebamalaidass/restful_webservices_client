package edu.luc.webservices.bean;

public class Login {
	
	private String userName;
	private String password;
	private int userid;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	@Override
	public String toString() {
		return "Login [userName=" + userName + ", password=" + password + ", userid=" + userid + "]";
	}
	
	

}
