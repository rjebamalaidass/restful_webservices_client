package edu.luc.webservices.bean;

import java.util.Date;

import javax.persistence.Column;

public class OrderOfPartner {
	
	private int productid;
	private int orderid;
	private String productname;
	private int quantity;
	private String orderfullfillmentstatus;
	private Date datefullfilled;
	private Date dateorderreceived;
	
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getOrderfullfillmentstatus() {
		return orderfullfillmentstatus;
	}
	public void setOrderfullfillmentstatus(String orderfullfillmentstatus) {
		this.orderfullfillmentstatus = orderfullfillmentstatus;
	}
	public Date getDatefullfilled() {
		return datefullfilled;
	}
	public void setDatefullfilled(Date datefullfilled) {
		this.datefullfilled = datefullfilled;
	}
	public Date getDateorderreceived() {
		return dateorderreceived;
	}
	public void setDateorderreceived(Date dateorderreceived) {
		this.dateorderreceived = dateorderreceived;
	}
	
	
	public int getOrderid() {
		return orderid;
	}
	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}
	@Override
	public String toString() {
		return "OrderOfPartner [productid=" + productid + ", orderid=" + orderid + ", productname=" + productname
				+ ", quantity=" + quantity + ", orderfullfillmentstatus=" + orderfullfillmentstatus
				+ ", datefullfilled=" + datefullfilled + ", dateorderreceived=" + dateorderreceived + "]";
	}
	
	

}
