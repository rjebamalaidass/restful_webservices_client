package edu.luc.webservices.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShippingStatus {
	
	@JsonProperty("shippingDate")
	private Date shippingDate;
	
	@JsonProperty("shippingStatus")
	private String shippingStatus;
	
	
	public Date getShippingDate() {
		return shippingDate;
	}
	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}
	public String getShippingStatus() {
		return shippingStatus;
	}
	public void setShippingStatus(String shippingStatus) {
		this.shippingStatus = shippingStatus;
	}
	@Override
	public String toString() {
		return "ShippingStatus [shippingDate=" + shippingDate + ", shippingStatus=" + shippingStatus + "]";
	}

}
